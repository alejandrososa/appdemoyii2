<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class AuthItemChild extends ActiveFixture
{
    public $modelClass = 'common\models\AuthItemChild';
    public $dataFile = 'common\tests\_data\auth_item_child.php';
}