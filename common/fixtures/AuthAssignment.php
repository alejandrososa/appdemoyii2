<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class AuthAssignment extends ActiveFixture
{
    public $modelClass = 'common\models\AuthAssignment';
    public $unloadFirst = ['common\fixtures\User'];
    public $dataFile = 'common\tests\_data\auth_assignment.php';
}