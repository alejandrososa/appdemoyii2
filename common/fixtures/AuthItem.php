<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class AuthItem extends ActiveFixture
{
    public $modelClass = 'common\models\AuthItem';
    public $dataFile = 'common\tests\_data\auth_item.php';
}