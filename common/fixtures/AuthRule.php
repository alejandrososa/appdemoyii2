<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class AuthRule extends ActiveFixture
{
    public $modelClass = 'common\models\AuthRule';
    public $dataFile = 'common\tests\_data\auth_rule.php';
}