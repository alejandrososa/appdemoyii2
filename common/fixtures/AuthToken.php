<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class AuthToken extends ActiveFixture
{
    public $modelClass = 'common\models\AuthToken';
    public $unloadFirst = ['common\fixtures\User'];
    public $dataFile = 'common\tests\_data\auth_token.php';
}