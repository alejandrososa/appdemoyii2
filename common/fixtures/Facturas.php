<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class Facturas extends ActiveFixture
{
    public $modelClass = 'common\models\Facturas';
    public $dataFile = 'common\tests\_data\facturas.php';

    public function listadoFacturas(){
        return $this->getData();
    }
}