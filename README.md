Yii 2 - Modulos de RBAC y Api RESTful 
=====================================

Demo Yii 2 con la plantilla avanzada para establecer un sistema de autenticación y autorización que pueda ser la base de nuestra aplicación, y de otras aplicaciones que construyamos en el futuro.

Consta de varios modulos: RBAC, Api RESTful, Facturas y Cuentas.

**RBAC:** Control de acceso basado en roles.
Documentación en [backend/modules/rbac/README.md](backend/modules/rbac/README.md)

**Api RESTful:** Tipo de arquitectura de desarrollo web que se apoya totalmente en el estándar HTTP.
Documentación en [frontend/modules/api/README.md](frontend/modules/api/README.md)

**Facturas:** Gestión de facturas ficticias, finalidad de gestionar algún tipo de datos.

**Cuentas:** Gestión de cuentas de usuarios.


TDD con Codeception
===================

Para ejecutar las pruebas unitarias, funcionales o de aceptación, lo primero que debes hacer es crear la estructura de modelos en la base de datos, lo mejor forma es creando una base de datos solo para nuestros tests llamada **demoyii2advanced_tests** y lo hacemos ejecutando las migraciones en la consola.


    echo "create database demoyii2advanced_tests charset utf8;" | mysql -u root -p
    php ./yii_test migrate
    
Ahora si, a ejecutar las pruebas funcionales de nuestra api

    vendor/bin/codecept run api -c frontend --colors 
    vendor/bin/codecept run api -c frontend --colors -vv (para más información de las pruebas)




Documentación Oficial
====================
Para desarrollar estos modulos se han seguidos los pasos de la documentación oficial de Yii 2.

http://www.yiiframework.com/doc-2.0/guide-db-migrations.html
http://www.yiiframework.com/doc-2.0/guide-tutorial-console.html
http://www.yiiframework.com/doc-2.0/guide-structure-modules.html
http://www.yiiframework.com/doc-2.0/yii-web-identityinterface.html
http://www.yiiframework.com/doc-2.0/guide-security-authentication.html
http://www.yiiframework.com/doc-2.0/guide-rest-quick-start.html
https://github.com/yiisoft/yii2-gii/blob/master/docs/guide/README.md

