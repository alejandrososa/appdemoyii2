<?php

use yii\db\Migration;

/**
 * Handles the creation of table `facturas`.
 */
class m161207_141852_create_facturas_table extends Migration
{
    public $tbl_usuarios;
    public $tbl_facturas;

    const TIPO_RESTRICT     = 'RESTRICT';
    const TIPO_CASCADE      = 'CASCADE';
    const TIPO_NO_ACTION    = 'NO ACTION';
    const TIPO_SET_DEFAULT  = 'SET DEFAULT';
    const TIPO_SET_NULL     = 'SET NULL';

    public function init()
    {
        parent::init();

        $this->tbl_usuarios = 'user';
        $this->tbl_facturas = 'facturas';
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tbl_facturas, [
            'id' => $this->primaryKey(),
            'codigo' => $this->string(20)->unique(),
            'cantidad' => $this->integer()->defaultValue(1)->notNull(),
            'concepto' => $this->string(70)->notNull(),
            'descripcion' => $this->text(),
            'empleado_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        // index columna `categorias`
        $this->createIndex(
            'idx-' . $this->tbl_facturas . '-empleado',
            $this->tbl_facturas,
            'empleado_id'
        );
        //clave foranea a `usuarios`
        $this->addForeignKey(
            'fk-' . $this->tbl_facturas . '-empleado',
            $this->tbl_facturas,
            'empleado_id',
            $this->tbl_usuarios,
            'id',
            self::TIPO_CASCADE,
            self::TIPO_NO_ACTION
        );


        $this->batchInsert($this->tbl_facturas,
            [
                'codigo',
                'cantidad',
                'concepto',
                'descripcion',
                'empleado_id',
                'created_at',
                'updated_at'
            ],
            [
                [ '1234F', 1, 'Pulido de zapatos', 'Pulido de zapatos nuevos', 3, '1481201184', '1481201579' ],
                [ '1235F', 3, 'Reparación de tuberías', 'Reparación de tuberías en todo el piso', 4, '1481201626', '1481201626' ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tbl_facturas);
    }
}
