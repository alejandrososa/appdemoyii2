<?php
namespace frontend\tests\Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use frontend\tests\ApiTester;

class Api extends \Codeception\Module
{
    /**
     * Formulario de autenticacion
     * @param $login
     * @param $password
     * @return array
     */
    public function formAutenticacion($login, $password)
    {
        return [
            'username' => $login,
            'password' => $password,
        ];
    }
}
