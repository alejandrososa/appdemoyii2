<?php

return [
    [
        'parent' => 'admin',
        'child' => 'crearFacturas'
    ],
    [
        'parent' => 'empleado',
        'child' => 'crearFacturas'
    ],
    [
        'parent' => 'vendedor',
        'child' => 'crearFacturas'
    ],
    [
        'parent' => 'admin',
        'child' => 'editarFacturasAjenas'
    ],
    [
        'parent' => 'empleado',
        'child' => 'editarFacturasAjenas'
    ],
    [
        'parent' => 'admin',
        'child' => 'editarFacturasPropias'
    ],
    [
        'parent' => 'empleado',
        'child' => 'editarFacturasPropias'
    ],
    [
        'parent' => 'vendedor',
        'child' => 'editarFacturasPropias'
    ],
    [
        'parent' => 'admin',
        'child' => 'eliminarFacturas'
    ],
];