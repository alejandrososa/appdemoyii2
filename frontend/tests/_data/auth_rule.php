<?php

return [
    [
        'name' => 'crearFacturas',
        'data' => 'O:29:"console\rbac\CrearFacturaRule":3:{s:4:"name";s:13:"crearFacturas";s:9:"createdAt";i:1481282616;s:9:"updatedAt";i:1481282616;}',
        'created_at' => '1481282616',
        'updated_at' => '1481282616',
    ],
    [
        'name' => 'editarFacturasAjenas',
        'data' => 'O:35:"console\rbac\EditarAjenaFacturaRule":3:{s:4:"name";s:20:"editarFacturasAjenas";s:9:"createdAt";i:1481282616;s:9:"updatedAt";i:1481282616;}',
        'created_at' => '1481282616',
        'updated_at' => '1481282616',
    ],[
        'name' => 'editarFacturasPropias',
        'data' => 'O:36:"console\rbac\EditarPropiaFacturaRule":3:{s:4:"name";s:21:"editarFacturasPropias";s:9:"createdAt";i:1481282616;s:9:"updatedAt";i:1481282616;}',
        'created_at' => '1481282616',
        'updated_at' => '1481282616',
    ],
    [
        'name' => 'eliminarFacturas',
        'data' => 'O:32:"console\rbac\EliminarFacturaRule":3:{s:4:"name";s:16:"eliminarFacturas";s:9:"createdAt";i:1481282616;s:9:"updatedAt";i:1481282616;}',
        'created_at' => '1481282616',
        'updated_at' => '1481282616',
    ],
];