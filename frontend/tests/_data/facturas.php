<?php

return [
    [
        'codigo' => '1234F',
        'cantidad' => 1,
        'concepto' => 'Pulido de zapatos',
        'descripcion' => 'Pulido de zapatos nuevos',
        'empleado_id' => 3,
        'created_at' => '1481201184',
        'updated_at' => '1481201579',
    ],
    [
        'codigo' => '1235F',
        'cantidad' => 3,
        'concepto' => 'Reparación de tuberías',
        'descripcion' => 'Reparación de tuberías en todo el piso',
        'empleado_id' => 4,
        'created_at' => '1481201626',
        'updated_at' => '1481201626',
    ]
];