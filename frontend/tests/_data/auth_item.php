<?php

return [
    [
        'name' => 'admin',
        'type' => 1,
        'description' => 'Puede acceder a todo',
        'rule_name' => null,
        'data' => null,
        'created_at' => '1481190449',
        'updated_at' => '1481197926'
    ],
    [
        'name' => 'crearFacturas',
        'type' => 2,
        'description' => 'Permiso para crear facturas',
        'rule_name' => 'crearFacturas',
        'data' => null,
        'created_at' => '1481190350',
        'updated_at' => '1481197926'
    ],
    [
        'name' => 'editarFacturasAjenas',
        'type' => 2,
        'description' => 'Permiso para actualizar facturas de otros usuarios',
        'rule_name' => 'editarFacturasAjenas',
        'data' => null,
        'created_at' => '1481190350',
        'updated_at' => '1481197926'
    ],
    [
        'name' => 'editarFacturasPropias',
        'type' => 2,
        'description' => 'Permiso para actualizar facturas propias',
        'rule_name' => 'editarFacturasPropias',
        'data' => null,
        'created_at' => '1481282616',
        'updated_at' => '1481282616'
    ],
    [
        'name' => 'eliminarFacturas',
        'type' => 2,
        'description' => 'Permiso para eliminar facturas',
        'rule_name' => 'eliminarFacturas',
        'data' => null,
        'created_at' => '1481282616',
        'updated_at' => '1481282616'
    ],
    [
        'name' => 'empleado',
        'type' => 1,
        'description' => 'Puede acceder panel administracion de empleado',
        'rule_name' => null,
        'data' => null,
        'created_at' => '1481282616',
        'updated_at' => '1481282616'
    ],
    [
        'name' => 'empresa',
        'type' => 1,
        'description' => 'Puede acceder panel administracion de empresa',
        'rule_name' => null,
        'data' => null,
        'created_at' => '1481282616',
        'updated_at' => '1481282616'
    ],
    [
        'name' => 'inversor',
        'type' => 1,
        'description' => 'Puede acceder estadisticas administracion',
        'rule_name' => null,
        'data' => null,
        'created_at' => '1481282616',
        'updated_at' => '1481282616'
    ],
    [
        'name' => 'particular',
        'type' => 1,
        'description' => 'Puede acceder panel administracion de un particular',
        'rule_name' => null,
        'data' => null,
        'created_at' => '1481282615',
        'updated_at' => '1481282615'
    ],
    [
        'name' => 'vendedor',
        'type' => 1,
        'description' => 'Puede acceder panel administracion de vendedor',
        'rule_name' => null,
        'data' => null,
        'created_at' => '1481282616',
        'updated_at' => '1481282616'
    ]
];