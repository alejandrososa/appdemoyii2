<?php
namespace frontend\tests;
use Codeception\Util\HttpCode;
use frontend\tests\ApiTester;
use common\fixtures\User as UserFixture;
use common\fixtures\AuthToken as UserTokenFixture;

/**
 * Class LoginCest
 * @package frontend\tests
 */
class LoginCest
{
    public function _before(ApiTester $I)
    {
        $I->haveFixtures([
            'users' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ]);

        $I->haveFixtures([
            'usertokens' => [
                'class' => UserTokenFixture::className(),
                'dataFile' => codecept_data_dir() . 'auth_token.php'
            ]
        ]);
    }

    // tests
    public function testErrorAlIntentarAccederSinUsuarioYClave(ApiTester $I)
    {
        $I->wantTo('acceder via API sin credenciales');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/acceso/login',$I->formAutenticacion('', ''));
        $I->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY); // 422
        $I->seeResponseIsJson();
        $I->seeResponseContains('[{"field":"username","message":"Username cannot be blank."},{"field":"password","message":"Password cannot be blank."}]');
    }

    public function testAdminAccedeCorrectamenteConUsuarioYClave(ApiTester $I)
    {
        $I->wantTo('acceder como admin via API con credenciales');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST("/acceso/login",$I->formAutenticacion('admin', '123456'));
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"access_token":"l1STFriemVGnSXEGkpNpWcY5XnKGBmSs"}');
    }
}