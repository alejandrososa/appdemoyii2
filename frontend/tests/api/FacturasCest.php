<?php
namespace frontend\tests;
use Codeception\Util\HttpCode;
use frontend\tests\ApiTester;
use common\fixtures\User as UserFixture;
use common\fixtures\AuthToken as UserTokenFixture;
use common\fixtures\AuthRule as AuthRuleFixture;
use common\fixtures\AuthItem as AuthItemFixture;
use common\fixtures\AuthItemChild as AuthItemChildFixture;
use common\fixtures\AuthAssignment as AuthAssignmentFixture;
use common\fixtures\Facturas as FacturasFixture;

/**
 * Class FacturasCest
 * @package frontend\tests
 */
class FacturasCest
{
    function _before(ApiTester $I)
    {
        $I->haveFixtures([
            'users' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ]);

        $I->haveFixtures([
            'usertokens' => [
                'class' => UserTokenFixture::className(),
                'dataFile' => codecept_data_dir() . 'auth_token.php'
            ]
        ]);

        $I->haveFixtures([
            'authrules' => [
                'class' => AuthRuleFixture::className(),
                'dataFile' => codecept_data_dir() . 'auth_rule.php'
            ]
        ]);

        $I->haveFixtures([
            'authitem' => [
                'class' => AuthItemFixture::className(),
                'dataFile' => codecept_data_dir() . 'auth_item.php'
            ]
        ]);

        $I->haveFixtures([
            'authitemchild' => [
                'class' => AuthItemChildFixture::className(),
                'dataFile' => codecept_data_dir() . 'auth_item_child.php'
            ]
        ]);

        $I->haveFixtures([
            'authassignment' => [
                'class' => AuthAssignmentFixture::className(),
                'dataFile' => codecept_data_dir() . 'auth_assignment.php'
            ]
        ]);
        
        $I->haveFixtures([
            'facturas' => [
                'class' => FacturasFixture::className(),
                'dataFile' => codecept_data_dir() . 'facturas.php'
            ]
        ]);
    }

    /**
     * Crud Factura
     * @param ApiTester $I
     * @param $uri
     * @param $token
     * @param array $factura
     * @param string $action 'GET', 'HEAD', 'POST', PUT', 'PATCH', 'DELETE'
     * @param $code
     * @param null $respuesta
     * @param string $enunciado lo que se desea probar
     * @return null
     */
    private function crudFactura(ApiTester $I, $uri, $token, $factura = [],
                                 $action = 'GET', $code, $respuesta = null, $enunciado = ''){

        if(empty($uri) && empty($token)){
            return null;
        }

        $I->wantTo($enunciado);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->amBearerAuthenticated($token);

        //'index' => ['GET', 'HEAD'],
        //'view' => ['GET', 'HEAD'],
        //'create' => ['POST'],
        //'update' => ['PUT', 'PATCH'],
        //'delete' => ['DELETE'],

        if ($action == 'GET') {
            $I->sendGET($uri, $factura);
        }elseif ($action == 'HEAD') {
            $I->sendHEAD($uri, $factura);
        }elseif ($action == 'POST') {
            $I->sendPOST($uri, $factura);
        }elseif ($action == 'PUT') {
            $I->sendPUT($uri, $factura);
        }elseif ($action == 'PATCH') {
            $I->sendPATCH($uri, $factura);
        }elseif ($action == 'DELETE') {
            $I->sendDELETE($uri, $factura);
        }else {
            $I->sendGET($uri, $factura);
        }

        $I->sendPOST($uri, $factura);
        $I->seeResponseIsJson();

        if(!empty($respuesta)){
            $I->seeResponseCodeIs($code);
        }
    }

    // tests
    public function testErrorAlIntentarVerFacturasSinToken(ApiTester $I)
    {
        $enunciado = 'Ver facturas sin credenciales obtiene estado 201 Unauthorized';
        $respuesta = [
            "name"=>"Unauthorized",
            "message"=>"Your request was made with invalid credentials.",
            "code"=>0,
            "status"=>401,
            "type"=>"yii\\web\\UnauthorizedHttpException"
        ];

        $this->crudFactura($I, '/factura', '', [], 'GET', HttpCode::UNAUTHORIZED, $respuesta, $enunciado);
    }

    public function testAccederComoAdminYVerListadoFacturas(ApiTester $I)
    {
        $enunciado = 'acceder al listado de facturas como admin obtiene estado 200 Ok';


        //obtengo el usuario admin de base de datos
        $usuarioAdmin = $I->grabFixture('users', 0); //posicion 0 en el array de usuarios de  Userfixture
        $token = $usuarioAdmin->authToken->token;

        //obtengo las facturas que hay en base de datos
        $facturas = $I->grabFixture('facturas');
        $respuesta = $facturas->listadoFacturas();

        //verifico si el admin puede acceder y si la info que obtengo del api es la misma que esta en base de datos
        $this->crudFactura($I, '/factura', $token, [], 'GET', HttpCode::OK, $respuesta, $enunciado);
    }

    public function testAccederComoAdminYVerDetalleDeUnaFactura(ApiTester $I)
    {
        $enunciado = 'acceder al detalle de la factura #1 como admin obtiene estado 403 Forbidden';

        //obtengo el usuario admin de base de datos
        $usuarioAdmin = $I->grabFixture('users', 0); //posicion 0 en el array de usuarios de  Userfixture
        $token = $usuarioAdmin->authToken->token;

        //obtengo la primera factura que hay en base de datos
        $facturas = $I->grabFixture('facturas');
        $respuesta = $facturas->listadoFacturas()[0];

        //verifico si el admin puede acceder y si la info que obtengo del api es la misma que esta en base de datos
        $this->crudFactura($I, '/factura/1', $token, [], 'GET', HttpCode::OK, $respuesta, $enunciado);
    }

    public function testAccederComoAdminYCrearUnaFactura(ApiTester $I)
    {
        //obtengo el usuario admin de base de datos
        $usuarioAdmin = $I->grabFixture('users', 0); //posicion 0 en el array de usuarios de  Userfixture
        $token = $usuarioAdmin->authToken->token;

        $nuevaFactura = [
            "codigo" => "1237F",
            "cantidad" => 1,
            "concepto" => "Migración de base de datos",
            "descripcion" => "Factura creada desde la api en una prueba funcional",
            "empleado_id" => $usuarioAdmin->id,
            "created_at" => 1481201184,
            "updated_at" => 1481201579
        ];

        //verifico si el admin puede acceder y si la info que obtengo del api es la misma que esta en base de datos
        $I->wantTo('crear una factura como admin obtiene estado 201 Created');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->amBearerAuthenticated($token);
        $I->sendPOST("/factura", $nuevaFactura);
        $I->seeResponseIsJson();
        $I->seeResponseCodeIs(201); // 201 (Created)
    }

    public function testAccederComoParticularObtieneErrorIntentandoCrearUnaFactura(ApiTester $I)
    {
        //obtengo el usuario particular de base de datos
        $usuarioAdmin = $I->grabFixture('users', 1); //posicion 1 en el array de usuarios de  Userfixture
        $token = $usuarioAdmin->authToken->token;

        $nuevaFactura = [
            "codigo" => "1237F",
            "cantidad" => 1,
            "concepto" => "Migración de base de datos",
            "descripcion" => "Factura creada desde la api en una prueba funcional",
            "empleado_id" => $usuarioAdmin->id,
            "created_at" => 1481201184,
            "updated_at" => 1481201579
        ];

        //verifico si el admin puede acceder y si la info que obtengo del api es la misma que esta en base de datos
        $I->wantTo('crear una factura como particular obtiene estado 403 Forbidden');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->amBearerAuthenticated($token);
        $I->sendPOST("/factura", $nuevaFactura);
        $I->seeResponseIsJson();
        $I->seeResponseCodeIs(HttpCode::FORBIDDEN); //403 (Forbidden)
    }
}